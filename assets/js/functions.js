var parseReposNames = function(data) {
    var repoNames=  [];
    for(var i = 0, l = data.length ; i < l ; i++){
       repoNames.push(data[i].name);
    }
    return repoNames;
};

var aggregateContributors = function(contributorListArr){
    var contributors = {};
    for (var i = 0, l = contributorListArr.length ; i < l ; i++){
        for (var j = 0, ll = contributorListArr[i].data.length ; j < ll ; j++){
            if(contributors[contributorListArr[i].data[j].login])
               contributors[contributorListArr[i].data[j].login].contributions += contributorListArr[i].data[j].contributions;
            else
               contributors[contributorListArr[i].data[j].login] = {login: contributorListArr[i].data[j].login, contributions: contributorListArr[i].data[j].contributions}
        }
    }
    return contributors;
};

var appendContributorDetails = function(contributorDetailsArr, aggregateContributors){
    for(var i = 0, l = contributorDetailsArr.length ; i < l ; i++){
        var current = aggregateContributors[contributorDetailsArr[i].data.login];
        current.avatarUrl = contributorDetailsArr[i].data.avatar_url;
        current.reposUrl = contributorDetailsArr[i].data.repos_url;
        current.publicRepos = contributorDetailsArr[i].data.public_repos;
        current.publicGists = contributorDetailsArr[i].data.public_gists;
        current.followers = contributorDetailsArr[i].data.followers;
        current.following = contributorDetailsArr[i].data.following;
        current.htmlUrl = contributorDetailsArr[i].data.html_url;
        current.name = contributorDetailsArr[i].data.name;
        current.blog = contributorDetailsArr[i].data.blog;
        current.location = contributorDetailsArr[i].data.location;
        current.email = contributorDetailsArr[i].data.email;
        current.bio = contributorDetailsArr[i].data.bio;
    }
    return Object.keys(aggregateContributors).map(function (key) { return aggregateContributors[key]; });
};

var parseContributorPublicRepos = function(reposArr){
    var ret = [];
    for(var i = 0, l = reposArr.length ; i < l ; i++){
        ret.push({
            repoName: reposArr[i].name,
            contributorsUrl: reposArr[i].contributors_url
        });
    }
    return ret;
}

var parseSelectedRepoContributorList = function(contributorArr){
    var ret = [];
    for(var i = 0, l = contributorArr.length ; i < l ; i++){
        ret.push({
            avatarUrl: contributorArr[i].avatar_url,
            login: contributorArr[i].login,
            htmlUrl: contributorArr[i].html_url,
            contributions: contributorArr[i].contributions
        });
    }
    return ret;
}