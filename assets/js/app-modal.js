app.controller('detailsModalCtrl', function ($scope, $rootScope, $modal, $log) {

    $scope.contributor = {};

    $rootScope.$on("invokeModal", function(event, param){
        $scope.contributor = param.contributor;
        $scope.open("lg");
    });
    
    $scope.open = function (size) {
        var modalInstance = $modal.open({
            templateUrl: 'assets/tpl/contributorDetailsModal.tpl.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            resolve: {
                contributor: function () {
                    return $scope.contributor;
                }
            }
        });

        modalInstance.result
            .then(
            // on ok click
            function (selectedItem) {},
            // on cancel click
            function () {});
    };
});

app.controller('ModalInstanceCtrl', function ($scope, $modalInstance, $log, contributor, modalDataFactory) {

    $scope.contributor = contributor;
    $scope.contributorRepoList = [];
    $scope.selectedRepoContributorList = [];
    $scope.selectedRepo = "";
    $scope.repoContributorListRequested = false;
    
    $scope.fetchingRepoList = false;
    $scope.fetchingContributorList = false;
    
    $scope.getRepoList = function(repoUrl){
        if($scope.contributorRepoList.length) return;
        $scope.fetchingRepoList = true;
        modalDataFactory.getRepoList(repoUrl).then(
            function(response){
                $scope.contributorRepoList = parseContributorPublicRepos(response.data);
                $scope.fetchingRepoList = false;
            },
            function(){
                $scope.fetchingRepoList = false;
                $log.error("Error receiving user repo list for modal")
            }
        );
    };
    
    $scope.getRepoContributorList = function(repoName, repoContributorUrl){
        $scope.selectedRepo = repoName;
        $scope.fetchingContributorList = true;
        modalDataFactory.getRepoContributorList(repoContributorUrl).then(
            function(response){
                $scope.selectedRepoContributorList = parseSelectedRepoContributorList(response.data);
                $scope.repoContributorListRequested = true;
                $scope.fetchingContributorList = false;
            },
            function(){
                $scope.repoContributorListRequested = true;
                $log.error("Error receiving contributor list of repo for modal")
            }
        );
    };
    
    $scope.ok = function () {};

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

app.factory('modalDataFactory', function($http){
    return{
        getRepoList: function(repoUrl){
            return $http.get(repoUrl + '?access_token=' + ACCESS_TOKEN,{cache: true});
        },
        getRepoContributorList: function(repoContributorUrl){
            return $http.get(repoContributorUrl + '?access_token=' + ACCESS_TOKEN,{cache: true});
        }
    }
});