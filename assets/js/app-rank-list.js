app.controller('requestCtrl', function($scope, $rootScope, $q, $log, data, contributorListFactory){
    // initial org name
    $scope.gitName = 'angular';
   
    $scope.requestMade = function(){
        $rootScope.$emit("orgRequestMade");
    };
    
    $scope.requestError = function(){
        $rootScope.$emit("requestError");
    };
    
    // load data
    $scope.getContributorList = function(){
        $scope.requestMade();
        var repoListPromise = contributorListFactory.getRepoList($scope.gitName);
        var aggregatedContributors = {};
        repoListPromise.then(
            function(response){
                var contributorListPromiseArray = contributorListFactory.getContributorList(parseReposNames(response.data), $scope.gitName);
                return $q.all(contributorListPromiseArray);
            },
            function(){
                $scope.requestError();
                $log.error("Error returning repo list");
            }
        ).then(
            function(response){
                aggregatedContributors = aggregateContributors(response);
                var contributorDetailsPromiseArray = contributorListFactory.getContributorDetailsList(aggregatedContributors);
                return $q.all(contributorDetailsPromiseArray);
            },
            function(){
                $scope.requestError();
                $log.error("Error returning contributor list");
            }
        ).then(
            function(response){
                data.setContributorList(appendContributorDetails(response, aggregatedContributors));
            },
            function(){
                $scope.requestError();
                $log.error("Error returning contributor details list");
            }
        );
    };
});

app.service('data', function($rootScope){
    var contributorList = [];
    
    return{
        setContributorList: function(arr){
            contributorList = arr;
        },
        getContributorList: function(){
            return contributorList;
        }
    };
});

app.controller('resultsCtrl', function($scope, $rootScope, $log, data){
    // view toggle
    $scope.fetchingRequest = false;
    $scope.requestError = false;
    $scope.firstRequestMade = false;
    
    $rootScope.$on("orgRequestMade", function(event, param){
        $scope.contributors = [];
        $scope.fetchingRequest = true;
        $scope.requestError = false;
    });
    
    $rootScope.$on("requestError", function(event, param){
        $scope.contributors = [];
        $scope.fetchingRequest = false;
        $scope.requestError = true;
    });
    
    //contributor list
    $scope.contributors = [];
    $scope.$watch(function () { return data.getContributorList(); }, function (newValue, oldValue) {
        if (newValue !== oldValue) {
            $scope.contributors = newValue;
            $scope.firstRequestMade = true;
            $scope.fetchingRequest = false;
        }
    });
    
    //sorting
    $scope.sortType = 'contributions';
    $scope.sortDesc = true;
    $scope.searchTerm = '';
    $scope.filterToggle = function(sortType){
        if($scope.sortType === sortType){
            $scope.sortDesc = !$scope.sortDesc;
        } else {
            $scope.sortType = sortType
            $scope.sortDesc = (sortType === 'login') ? false : true;
        }
    }
    
    // pagination
    $scope.elemPerPageSelect = [16, 32, 64];
    $scope.elemPerPage = $scope.elemPerPageSelect[0];
    $scope.currentPage = 1;
    
    $scope.showContributorDetails = function(contributor){
        $rootScope.$emit("invokeModal", {contributor:contributor});
    };
});
    
app.factory('contributorListFactory', function ($q, $http) {
  return {
    getRepoList: function (gitName) {
        var promise = $http.get('https://api.github.com/orgs/' + gitName + '/repos?access_token=' + ACCESS_TOKEN, {cache: true});
        return promise;
    },
    getContributorList: function (repoList, gitName){
        var promiseArr = [];
        for(var i = 0, l = repoList.length ; i < l ; i++){
            promiseArr.push(
                $http.get('https://api.github.com/repos/' + gitName + '/' + repoList[i] + '/contributors?access_token=' + ACCESS_TOKEN, {cache: true})
            );
        }
        return promiseArr;
    },
    getContributorDetailsList: function (contributorList){
        var promiseArr = [];
        for (contributor in contributorList){
            promiseArr.push(
                $http.get('https://api.github.com/users/' + contributor + '?access_token=' + ACCESS_TOKEN, {cache: true})
            );
        }
        return promiseArr;
    }
  }
});